class Hangman

  def initialize(players = {:guesser => player1, :referee => player2})
    @guesser = players[:guesser]
    @referee = players[:referee]
  end

  attr_reader :guesser, :referee, :board

  def setup
    secret_length = @referee.pick_secret_word
    @guesser.register_secret_length(secret_length)
    @board = "_" * secret_length
  end

  def take_turn
    guess = @guesser.guess(board)
    indices = @referee.check_guess(guess)
    update_board(guess,indices)
    @guesser.handle_response
  end

  def update_board(guess,indices)
    if indices != []
      indices.each do |idx|
        @board[idx]=guess
      end
    end
  end

  def play
    setup
    until over?
      take_turn
      update_board
    end
  end

  def over?
    @board.include?("_") == false
  end

end

class HumanPlayer

  def initialize(dictionary=File.readlines("lib/dictionary.txt"))
    @dictionary= dictionary
    @secret_word=""
    @pick_secret_word=nil
  end

  attr_accessor :dictionary

  def secret_word(dictionary)
    @secret_word = dictionary[rand(dictionary.length-1)]
  end

  def pick_secret_word #length of secret word
    puts "How long is the secret word?"
    length = gets.chomp
  end

  def check_guess(letter)
    indices = []
    @secret_word.chars.each_with_index do |el,idx|
      if letter == el
        indices.push(idx)
      end
    end
    indices
  end

  def register_secret_length(length)
    puts "The length of the word is #{length}"
    @candidate_words = @dictionary.select do |word|
      word.length == length
    end
  end

  def guess(board)
    puts "What's your guess?"
    guess = gets.chomp
  end

  def handle_response(guess,indices)
    if indices == []
      puts "The word doesn't have #{guess}"
      @candidate_words.reject! do |word|
        word.include?(guess)
      end
    else
      puts "#{guess} was found at these indices: #{indices}"
      indices.each do |idx|
        @candidate_words.reject! do |word|
          word.chars[idx] != guess
        end
      end
      temp_candidates=@candidate_words
      @candidate_words.each do |word|
        word.chars.each_with_index do |char,idx|
          if char == guess && indices.include?(idx)==false
            temp_candidates.delete(word)
          end
        end
        @candidate_words=temp_candidates
      end
    end
  end

end

class ComputerPlayer

  def initialize(dictionary=File.readlines("lib/dictionary.txt"))
    @dictionary= dictionary
    @secret_word=""
    @pick_secret_word=nil
    @candidate_words = []
  end

  attr_accessor :dictionary, :candidate_words, :secret_word,
    :pick_secret_word

  def secret_word(dictionary)
    @secret_word = dictionary.sample
  end

  def pick_secret_word
    @pick_secret_word = self.secret_word(dictionary).length
  end

  def check_guess(letter)
    indices = []
    @secret_word.chars.each_with_index do |el,idx|
      if letter == el
        indices.push(idx)
      end
    end
    indices
  end

  def register_secret_length(length)
    puts "The length of the word is #{length}"
    @candidate_words = @dictionary.select do |word|
      word.length == length
    end
  end

  def guess(board)
    most_common_letter(board)
  end

  def most_common_letter(board)
    counter = Hash.new(0)
    @candidate_words.each do |word|
      word.chars.each do |letter|
        counter[letter]+=1
      end
    end
    counter.select! {|k,v| board.include?(k)==false}
    counter.sort_by {|k,v| v}.last[0]
  end

  def handle_response(guess,indices)
    if indices == []
      @candidate_words.reject! do |word|
        word.include?(guess)
      end
    else
      indices.each do |idx|
        @candidate_words.reject! do |word|
          word.chars[idx] != guess
        end
      end
      temp_candidates=@candidate_words
      @candidate_words.each do |word|
        word.chars.each_with_index do |char,idx|
          if char == guess && indices.include?(idx)==false
            temp_candidates.delete(word)
          end
        end
        @candidate_words=temp_candidates
      end
    end
  end

end

if $PROGRAM_NAME == __FILE__
  Hangman.new(HumanPlayer.new,ComputerPlayer.new).play
end
